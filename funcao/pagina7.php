<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php

    function soma(int ...$valores){
        return array_sum($valores);
    }

    echo soma(2,2);
    echo "<br>";
    echo soma(25, 33);
    echo "<br>";
    echo soma (1.5,3.2);
    echo "<br>";

    ?>

</body>
</html>