<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

class Cadastro{
    private $nome;
    private $email;
    private $senha;
    public function getNome():string{
        return $this->nome;
    }
    public function getEmail():string{
        return $this->email;
    }
    public function getSenha():string{
        return $this->senha;
    }

    public function setNome($nome){
        $this->nome=$nome;
    }

    public function setEmail($email){
        $this->email=$email;
    }
    public function setSenha($senha){
        $this->senha=$senha;
    }

    public function  __toString(){
        return json_encode(array(
            "nome"=>$this->getNome(),
            "email"=>$this->getEmail(),
            "senha"=>$this->getSenha()
        ));
    }

}
?>

</body>
</html>