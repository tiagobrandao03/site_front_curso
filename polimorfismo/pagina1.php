<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
abstract class Animal{
    public function falar(){
        return "Som";
    }

    public function mover(){
        return "Anda";
    }
    
}

class Cachorro extends Animal{
    public function falar(){
        return "Late";
    }
}

class Gato extends Animal{
    public function falar(){
        return "mia";
    }
}

class Passaro extends Animal{
    public function falar(){
        return "Canta";
    }   
    public function mover(){
        return "Voa".parent::mover();
    }
}

$pluto = new  Cachorro();
echo $pluto ->falar()."<br/>";
echo $pluto ->mover()."<br/>";

echo".........................<br/>";

$garfield = new  Gato();
echo $garfield ->falar()."<br/>";
echo $garfield ->mover()."<br/>";

echo".........................<br/>";

$ave = new  Passaro();
echo $ave ->falar()."<br/>";
echo $ave ->mover()."<br/>";
?>

</body>
</html>