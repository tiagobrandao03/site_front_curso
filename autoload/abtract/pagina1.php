<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

function incluirClasses($nomeClasse){
    if(file_exists($nomeClasse.".php")===true){
        require_once($nomeClasse.".php");
    }
}

spl_autoload_register("incluirClasses");
spl_autoload_register(function($nomeClasses){
    if(file_exists("abstrata".DIRECTORY_SEPARATOR.$nomeClasse.".php")===true){
        require_once("abstrata".DIRECTORY_SEPARATOR.    $nomeClasse.".php");   
    }
}); 

$carro = new DelRey();
$carro->acelerar(80);

?>

</body>
</html>