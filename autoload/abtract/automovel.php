<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

interface Veiculo{
    public function acelerar($velocidade);
    public function frenar($velocidade);
    public function trocarMarcha($marcha);
}

abstract class Automovel  implements  Veiculo{
    public  function  acelerar($velocidade){
        echo "O veiculo  acelerou  até".$velocidade."km/h";
    }

    public function frenar($velocidade){
        echo "O veiculo frenou até ".$velocidade . "km/h";
    } 
    public function trocarMarcha($marcha){
        echo "O veiculo engatou a marcha" .$marcha;
    }
}
?>

</body>
</html>