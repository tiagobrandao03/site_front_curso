<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
    class Pessoa{
        public $nome;
        public function falar(){
            return "o meu nome é".$this->nome;
        }
    }
    $glaucio=new Pessoa();
    $glaucio->nome="Glaucio Daniel";
    echo $glaucio->falar();
    ?>

</body>
</html>