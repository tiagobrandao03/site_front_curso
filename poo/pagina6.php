<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
class Pessoa {
    public $nome ="Rasmus Lerdof";
    protected $idade=48;
    private $senha="123456";

    public function verDados(){
        echo $this->nome."<br/>";
        echo $this->idade."<br/>";
        echo $this->senha."<br/>";   
    }
}

class Programador extends Pessoa{
    public function verDados(){

        echo get_class($this)."<br/>";

        echo $this->nome."<br/>";
        echo $this->idade."<br/>";
        echo $this->senha."<br/>";   
    }
}

$objeto = new Programador();

$objeto->verDados();
?>

</body>
</html>