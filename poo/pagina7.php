<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
class Documento{
    private $numero;
    public function getNumero(){
        return $this ->numero;
    }
    public function setNumero($n){
        $this->numero=$n;
    }
}

class CPF extends Documento{
    public function validar():bool{
        $numeroCPF=$this->getNumero();

        return true;
    }
}

$doc = new CPF();
$doc->setNumero("111222333-44");
var_dump($doc->validar());
echo "<br/>";
echo $doc->getNumero();
?>

</body>
</html>